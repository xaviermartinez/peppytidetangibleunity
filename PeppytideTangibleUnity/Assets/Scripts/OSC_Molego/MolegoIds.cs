using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MolegoIds {

	public Dictionary<int, MolegoLink> idToType = new Dictionary<int, MolegoLink>();

	public MolegoIds() {
		readIdsFromPath();
	}
	public void readIdsFromPath() {
		string path = Application.streamingAssetsPath + "/NomenclatureMolegoIds.txt";

		using (StreamReader sr = new StreamReader(path))
		{
			string line;
			while ((line = sr.ReadLine()) != null)
			{
				try {
					if (line.StartsWith("#")) {
						continue;
					}
					string[] splits = line.Split(new [] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
					int id = int.Parse(splits[0]);
					int idM = int.Parse(splits[2]);
					MolegoLink mol = new MolegoLink(id, idM, splits[1], splits[3]);
					idToType[id] = mol;
				}
				catch {
					Debug.LogError("Ignoring line '" + line + "'");
				}
			}
		}

	}
}

public class MolegoLink {

	public enum PartType {
		Amide,
		Calpha,
		Radical
	}
	public enum LinkType {
		C,
		N,
		R
	}

	public int idMol;
	public int idLink;
	public PartType partType;
	public LinkType linkType;

	public MolegoLink(int id, int idM, string part, string link) {
		idLink = id;
		idMol = idM;
		partType = stringToPartType(part);
		linkType = stringToLinkType(link);
	}

	public bool isR() {
		if (partType == PartType.Calpha) {
			if (linkType == LinkType.R) {
				return true;
			}
		}
		else if (partType == PartType.Radical) {
			if (linkType == LinkType.R) {
				return true;
			}
		}
		return false;
	}
	public bool isLeft() {
		if (partType == PartType.Amide) {
			if (linkType == LinkType.C) {
				return true;
			}
			return false;
		}
		if (partType == PartType.Calpha) {
			if (linkType == LinkType.C) {
				return false;
			}
			return true;
		}

		return false;
	}


	PartType stringToPartType(string s) {
		switch (s) {
		case "amide":
			return PartType.Amide;
		case "calpha":
		case "CA":
			return PartType.Calpha;
		case "R":
		case "radical":
			return PartType.Radical;
		default:
			throw new System.Exception("Unknown type '" + s + "'");
		}
	}
	LinkType stringToLinkType(string s) {
		switch (s) {
		case "R":
			return LinkType.R;
		case "C":
			return LinkType.C;
		case "N":
			return LinkType.N;
		default:
			throw new System.Exception("Unknown type '" + s + "'");
		}
	}
}