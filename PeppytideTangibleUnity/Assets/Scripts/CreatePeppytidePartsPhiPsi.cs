using UnityEngine;
using System.Collections.Generic;


public class CreatePeppytidePartsPhiPsi : MonoBehaviour {

	public bool usePhysics = true;

	public int nbAA = 5;
	public List<float> phiPsi = new List<float>();

	private int createdAA = 0;
	private Transform previousAA;

	private static float friction = 0.5f;
	private static float weightAmide = 5.47f;
	private static float weightCalpha = 7.44f;
	private static float weightMethyl = 2.68f;

	private List<GameObject> goToRotate = new List<GameObject>();
	private List<GameObject> aminoAcids = new List<GameObject>();


	void Start(){

		//TEST
			for(int i=0;i<nbAA;i++){
				phiPsi.Add(-60.0f);
				phiPsi.Add(-45.0f);
			}

		//

		if(phiPsi.Count < 2 * nbAA)
			Debug.LogError("Provide enough phi and psi angles ("+nbAA+" amino acids need "+(2*nbAA)+" phi and psi angles)");

		float anglephi = phiPsi[0];
		float anglepsi = phiPsi[1];
		if(createdAA == 0){//No AA already there
			aminoAcids.Add(AddAAbase(anglephi,anglepsi));
			createdAA++;
			for(int i=0;i<nbAA-1;i++){
				anglephi = phiPsi[createdAA*2];
				anglepsi = phiPsi[createdAA*2 + 1];
				aminoAcids.Add(AddAA(createdAA-1,createdAA%2==0,anglephi,anglepsi));
				createdAA++;
			}
		}
		else{ // AA already created => add more
			for(int i=0;i<nbAA;i++){
				anglephi = phiPsi[createdAA*2];
				anglepsi = phiPsi[createdAA*2 + 1];
				aminoAcids.Add(AddAA(createdAA-1,createdAA%2==0,anglephi,anglepsi));
				createdAA++;
			}
		}
	}

	public GameObject AddAAbase(float phi,float psi){
		GameObject peptide = new GameObject("PeppytidePeptide");
		GameObject FirstAA = new GameObject ("AA(1)");
		FirstAA.transform.parent = peptide.transform;

		GameObject Calpha = createCalpha(FirstAA.transform, usePhysics);
		GameObject amideC = createAmide(FirstAA.transform,true, usePhysics);
		GameObject amideN = createAmide(FirstAA.transform,false, usePhysics);
		GameObject sideChain = createSideChain(FirstAA.transform,"R_AlaninePrefab", usePhysics);


		attachAmideToCA(amideN, false, Calpha, usePhysics);
		attachSideChainToCa(sideChain, Calpha, usePhysics);
		attachAmideToCA(amideC, true, Calpha, usePhysics);

		goToRotate.Add(Calpha);
		goToRotate.Add(sideChain);
		goToRotate.Add(amideC);

		setPhi(amideN, Calpha, amideC, phi,goToRotate);

		goToRotate.Clear();
		goToRotate.Add(amideC);
		setPsi(amideN, Calpha, amideC, psi, goToRotate);

		previousAA = FirstAA.transform;
		return FirstAA;

	}

	public GameObject AddAA(int previous,bool CorN,float phi,float psi){
		GameObject NewAA = new GameObject ("AA("+(previous+2).ToString()+")");
		NewAA.transform.parent = previousAA.transform.parent;

		GameObject Calpha = createCalpha(NewAA.transform, usePhysics);
		GameObject amide = createAmide(NewAA.transform,true, usePhysics);
		GameObject sideChain = createSideChain(NewAA.transform,"R_AlaninePrefab", usePhysics);

		GameObject previousAmide;
		previousAmide = previousAA.Find("AmideC").gameObject;


		attachCAToAmide(Calpha, previousAmide, false, usePhysics);
		attachSideChainToCa(sideChain, Calpha, usePhysics);
		attachAmideToCA(amide, true, Calpha, usePhysics);

		goToRotate.Clear();
		goToRotate.Add(Calpha);
		goToRotate.Add(sideChain);
		goToRotate.Add(amide);


		setPhi(previousAmide, Calpha, amide, phi, goToRotate);

		goToRotate.Clear();
		goToRotate.Add(amide);

		setPsi(previousAmide, Calpha, amide, psi, goToRotate);


		previousAA = NewAA.transform;
		return NewAA;


	}
	public static GameObject createCalpha(Transform par, bool usePhysics){
		GameObject go = Instantiate (Resources.Load ("CalphaPrefab")) as GameObject;
		go.name = go.name.Replace("Prefab(Clone)", string.Empty);
		go.transform.parent = par;

		if(usePhysics){
			Rigidbody caRb = go.AddComponent<Rigidbody>();
			caRb.useGravity = false;
			caRb.drag = friction;
			caRb.mass = weightCalpha;
		}

		return go;
	}
	public static GameObject createSideChain(Transform par, string sideChainName, bool usePhysics){

		GameObject methyl = Instantiate (Resources.Load ("SideChains/"+sideChainName)) as GameObject;
		methyl.name = methyl.name.Replace("Prefab(Clone)", string.Empty);
		methyl.transform.parent = par;

		if(usePhysics){
			Rigidbody scRb = methyl.AddComponent<Rigidbody>();
			scRb.useGravity = false;
			scRb.drag = friction;
			//TODO change this !
			scRb.mass = weightMethyl;
		}



		return methyl;
	}
	public static GameObject createAmide(Transform par, bool CorN, bool usePhysics){
		GameObject amide = Instantiate (Resources.Load ("AmidePrefab")) as GameObject;
		if(CorN)
			amide.name = amide.name.Replace("Prefab(Clone)", "C");
		else
			amide.name = amide.name.Replace("Prefab(Clone)", "N");

		amide.transform.parent = par;

		if(usePhysics){
			Rigidbody amideRb = amide.AddComponent<Rigidbody>();
			amideRb.useGravity = false;
			amideRb.drag = friction;
			amideRb.mass = weightAmide;
		}

		return amide;
	}
	public void attachSideChainToCa(GameObject sc, GameObject ca, bool usePhysicsLink=false){

		Transform sclinkint = sc.transform.Find("Links/intlinkC1");
		Transform sclinkext = sc.transform.Find("Links/extlinkC1");

		Transform CalinkScint = ca.transform.Find("Links/intlink3");
		Transform CalinkScext = ca.transform.Find("Links/extlink3M");

		//Orient Side chain on Calpha
		alignParts(sclinkint,sclinkext,CalinkScint,CalinkScext, sc.transform);

		if(usePhysicsLink){

			//Link it with hinge joint
			HingeJoint SCToCa = sc.AddComponent<HingeJoint>();
			SCToCa.connectedBody = ca.GetComponent<Rigidbody>();
			SCToCa.anchor = sc.transform.InverseTransformPoint(sclinkext.position);
			SCToCa.axis = (sclinkext.localPosition - sclinkint.localPosition);

		}
	}
	
	public void attachAmideToCA(GameObject amide, bool isClink, GameObject ca, bool usePhysicsLink=false){
		
		Transform amideLinkInt;
		Transform amideLinkExt;
		Transform CaLinkInt;
		Transform CaLinkExt;

		if(isClink){
			//Orient AmideC on Calpha
			amideLinkInt = amide.transform.Find("Links/intlink2");
			amideLinkExt = amide.transform.Find("Links/extlink2C");
			CaLinkInt = ca.transform.Find("Links/intlink1");
			CaLinkExt = ca.transform.Find("Links/extlink1C");
		}
		else{
			//Orient AmideN on Calpha
			amideLinkInt = amide.transform.Find("Links/intlink1");
			amideLinkExt = amide.transform.Find("Links/extlink1N");
			CaLinkInt = ca.transform.Find("Links/intlink2");
			CaLinkExt = ca.transform.Find("Links/extlink2N");
		}
		alignParts(amideLinkInt,amideLinkExt,CaLinkInt,CaLinkExt, amide.transform);	

		if(usePhysicsLink){
			HingeJoint AToCa = amide.AddComponent<HingeJoint>();
			AToCa.connectedBody = ca.GetComponent<Rigidbody>();
			AToCa.anchor = amide.transform.InverseTransformPoint(amideLinkInt.position);
			AToCa.axis = amideLinkExt.localPosition - amideLinkInt.localPosition;
		}

	}

	public void attachCAToAmide(GameObject ca, GameObject prevAmide, bool isClink, bool usePhysicsLink=false){
		Transform amideLinkInt;
		Transform amideLinkExt;
		Transform CaLinkInt;
		Transform CaLinkExt;

		if(isClink){
			//Orient AmideC on Calpha
			amideLinkInt = prevAmide.transform.Find("Links/intlink2");
			amideLinkExt = prevAmide.transform.Find("Links/extlink2C");
			CaLinkInt = ca.transform.Find("Links/intlink1");
			CaLinkExt = ca.transform.Find("Links/extlink1C");
		}
		else{
			//Orient AmideN on Calpha
			amideLinkInt = prevAmide.transform.Find("Links/intlink1");
			amideLinkExt = prevAmide.transform.Find("Links/extlink1N");
			CaLinkInt = ca.transform.Find("Links/intlink2");
			CaLinkExt = ca.transform.Find("Links/extlink2N");
		}

		alignParts(CaLinkInt,CaLinkExt,amideLinkInt,amideLinkExt, ca.transform);

		if(usePhysicsLink){
			HingeJoint AToCa = ca.AddComponent<HingeJoint>();
			AToCa.connectedBody = prevAmide.transform.GetComponent<Rigidbody>();
			AToCa.anchor = ca.transform.InverseTransformPoint(CaLinkExt.position);
			AToCa.axis = ca.transform.InverseTransformPoint(CaLinkInt.position) - ca.transform.InverseTransformPoint(CaLinkExt.position);

		}
	}

	public void alignParts(Transform intA,Transform extA,Transform intTarget,Transform extTarget,Transform tomove){

		Vector3 VecA = (extA.position - intA.position);
		Vector3 VecTarget = -(extTarget.position - intTarget.position);

		//Compute rotation from one vector to the other
		tomove.rotation = Quaternion.FromToRotation(VecA, VecTarget)* tomove.rotation;

		tomove.position = tomove.position + (extTarget.position - extA.position);//Glue both parts
		tomove.position = tomove.position + (VecTarget.normalized) * (-0.01f);//Add a small gap between the parts
	}


	public void setPhi(GameObject prevAmide, GameObject ca, GameObject amide, float newPhi, List<GameObject> goToRotate){

		Transform c1t = prevAmide.transform.Find("Centers/C_center");
		Transform n1t = prevAmide.transform.Find("Centers/N_center");
		Transform cat = ca.transform.Find("Centers/C_center");
		Transform n2t = amide.transform.Find("Centers/N_center");
		Transform c2t = amide.transform.Find("Centers/C_center");


		float curPhi = calc_dihedral(c1t.position,n1t.position,cat.position,c2t.position)*Mathf.Rad2Deg;

		float savePhi = curPhi;

		Vector3 CaLinkInt = ca.transform.Find("Links/intlink2").position;

		Vector3 amideLinkNExt = cat.position;
		Vector3 amideLinkNInt = n1t.position;

		foreach(GameObject go in goToRotate){
			go.transform.RotateAround(CaLinkInt, amideLinkNExt-amideLinkNInt,curPhi-newPhi);
		}


	}
	public void setPsi(GameObject prevAmide, GameObject ca, GameObject amide, float newPsi, List<GameObject> goToRotate){

		Transform n1t = prevAmide.transform.Find("Centers/N_center");
		Transform c1t = prevAmide.transform.Find("Centers/C_center");
		Transform cat = ca.transform.Find("Centers/C_center");
		Transform n2t = amide.transform.Find("Centers/N_center");
		Transform c2t = amide.transform.Find("Centers/C_center");


		Vector3 CaLinkInt = ca.transform.Find("Links/intlink1").position;

		Vector3 amideLinkCExt = cat.position;
		Vector3 amideLinkCInt = c2t.position;


		float curPsi = calc_dihedral(n1t.position,cat.position,c2t.position,n2t.position)*Mathf.Rad2Deg;

		foreach(GameObject go in goToRotate){
			go.transform.RotateAround(CaLinkInt,amideLinkCInt - amideLinkCExt,curPsi-newPsi);
		}

	}

	public void setPhiAA(float newPhi, GameObject aa){
		goToRotate = listObjectsAfterAA(aa);
		GameObject prevAmide = null;
		int id = aminoAcids.IndexOf(aa);
		if(id == 0)
			prevAmide = aa.transform.Find("AmideN").gameObject;
		else
			prevAmide = aminoAcids[id-1].transform.Find("AmideC").gameObject;

		GameObject ca = aa.transform.Find("Calpha").gameObject;
		GameObject amide = aa.transform.Find("AmideC").gameObject;
		GameObject sideChain = findSideChain(aa);

		goToRotate.Add(aa);

		setPhi(prevAmide, ca, amide, newPhi, goToRotate);

	}
	public void setPhiAA(float newPhi, int idAA){
		setPhiAA(newPhi, aminoAcids[idAA]);
	}

	public void setPsiAA(float newPsi, GameObject aa){
		goToRotate = listObjectsAfterAA(aa);
		GameObject prevAmide = null;
		int id = aminoAcids.IndexOf(aa);
		if(id == 0)
			prevAmide = aa.transform.Find("AmideN").gameObject;
		else
			prevAmide = aminoAcids[id-1].transform.Find("AmideC").gameObject;

		GameObject ca = aa.transform.Find("Calpha").gameObject;
		GameObject amide = aa.transform.Find("AmideC").gameObject;

		goToRotate.Add(amide);

		setPsi(prevAmide, ca, amide, newPsi, goToRotate);
	}
	public void setPsiAA(float newPsi, int idAA){
		setPsiAA(newPsi, aminoAcids[idAA]);
	}


	List<GameObject> listObjectsAfterAA(GameObject aa){
		List<GameObject> res = new List<GameObject>();
		int id = aminoAcids.IndexOf(aa);

		for(int i = id+1; i < aminoAcids.Count; i++){
			res.Add(aminoAcids[i]);
		}
		return res;
	}

	List<GameObject> listObjectsBeforeAA(GameObject aa){
		List<GameObject> res = new List<GameObject>();
		int id = aminoAcids.IndexOf(aa);

		for(int i = id-1; i >= 0; i--){
			res.Add(aminoAcids[i]);
		}
		return res;
	}
	GameObject findSideChain(GameObject aa){
		foreach(Transform t in aa.transform){
			if(t.name.StartsWith("R_"))
				return t.gameObject;
		}
		return null;
	}
	float calc_dihedral(Vector3 v1,Vector3 v2, Vector3 v3, Vector3 v4){
		Vector3 ab = v1-v2;
		Vector3 cb = v3-v2;
		Vector3 db = v4-v3;

		Vector3 u = Vector3.Cross(ab,cb);
		Vector3 v = Vector3.Cross(db,cb);
		Vector3 w = Vector3.Cross(v,u);

		float angle = get_angle2(u,v);//Vector3.Angle(u,v);
		if (get_angle2(cb,w) > 0.001)
			angle = -angle;
		return angle;

	}
	float get_angle2(Vector3 v1,Vector3 v2){
		float s = Vector3.Cross(v1,v2).magnitude;
		float c = Vector3.Dot(v1,v2);
		float angle = Mathf.Atan2(s,c);
		return angle;
	}


}
