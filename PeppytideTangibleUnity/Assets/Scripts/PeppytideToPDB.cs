﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class PeppytideToPDB : ScriptableWizard {
	
	public GameObject PeptideParent;
	// public float peppytideScaleFactor = 0.1069839f;
	private readonly Dictionary<string, string> AA3_to_1 = new Dictionary<string,string>()
		{
		{"ALA","A"},
		{"ARG","R"},
		{"ASN","N"},
		{"ASP","D"},
		{"CYS","C"},
		{"GLU","E"},
		{"GLN","Q"},
		{"GLY","G"},
		{"HIS","H"},
		{"ILE","I"},
		{"LEU","L"},
		{"LYS","K"},
		{"MET","M"},
		{"PHE","F"},
		{"PRO","P"},
		{"SER","S"},
		{"THR","T"},
		{"TRP","W"},
		{"TYR","Y"},
		{"VAL","V"}};

	[MenuItem ("PeppytideTools/PeppytideToPDB")]
	static void CreateWizard () {
		ScriptableWizard.DisplayWizard<PeppytideToPDB>("Peppytide to PDB (without lateral chains)", "Go");
	}
	void OnWizardCreate () {

		if(PeptideParent != null){
			int nbAtom = 0;
			int minId = int.MaxValue;
			int maxId = int.MinValue;
			StringBuilder toprint = new StringBuilder();
			Dictionary<int,StringBuilder> stringByRes = new Dictionary<int,StringBuilder>();

			foreach(Transform aa in PeptideParent.transform){
				int resid = int.Parse(aa.name.Split(new [] { '(', ')' })[1]);
				foreach(Transform t in aa){
					if(t.name.Contains("Calpha") || t.name.Contains("Amide")){
						Transform sphereParent = t.Find("Spheres");
						foreach(Transform atom in sphereParent){
							string aa3 = "ALA";//only ALA for now
							Vector3 pos = atom.position ;//* peppytideScaleFactor;


							string atomType = null;
							bool isnextAtom = false;

							int modifiedResId = resid;

							if(t.name.Contains("Amide") && (atom.name.Contains("Hydrogen") || atom.name.Contains("Nitrogen")))
								isnextAtom = true;


							if(resid != 1){
								if(isnextAtom)
									modifiedResId = resid+1;
							}
							else{
								if(t.name.Contains("AmideN") && (atom.name.Contains("Carbon") || atom.name.Contains("Oxygen")))
									modifiedResId = resid-1;

								else if(t.name.Contains("AmideC") && (atom.name.Contains("Nitrogen") || atom.name.Contains("Hydrogen")))
									modifiedResId = resid+1;
								
							}


							if(modifiedResId < minId)
								minId = modifiedResId;
							if(modifiedResId > maxId)
								maxId = modifiedResId;


							addToResidueString(stringByRes,modifiedResId,"ATOM  ");
							addToResidueString(stringByRes,modifiedResId,string.Format("{0,5}",nbAtom.ToString("D")));

							if(t.name.Contains("Calpha")){
								if(atom.name.Contains("Carbon")){
									addToResidueString(stringByRes,modifiedResId,"  CA  ");
									atomType = "C";
								}

								if(atom.name.Contains("Hydrogen")){//an "else" should be enough
									addToResidueString(stringByRes,modifiedResId,"  HA  ");
									atomType = "H";
								}
							}
							
							if(t.name.Contains("Amide")){

								if(atom.name.Contains("Hydrogen")){
									isnextAtom = true;
									addToResidueString(stringByRes,modifiedResId,"  H   ");
									atomType = "H";
								}

								else if(atom.name.Contains("Nitrogen")){
									isnextAtom = true;
									addToResidueString(stringByRes,modifiedResId,"  N   ");
									atomType = "N";
								}

								else if(atom.name.Contains("Carbon")){
									addToResidueString(stringByRes,modifiedResId,"  C   ");
									atomType = "C";
								}

								else if(atom.name.Contains("Oxygen")){
									atomType = "O";
									addToResidueString(stringByRes,modifiedResId,"  O   ");
								}

							}


							addToResidueString(stringByRes,modifiedResId,aa3);
							addToResidueString(stringByRes,modifiedResId," A");
							if(resid != 1){
								if(isnextAtom){
									addToResidueString(stringByRes,modifiedResId,string.Format("{0,4}",(resid+1).ToString("D")));
								}
								else
									addToResidueString(stringByRes,modifiedResId,string.Format("{0,4}",resid.ToString("D")));
							}
							else{
								if(t.name.Contains("AmideN") && (atom.name.Contains("Carbon") || atom.name.Contains("Oxygen"))){
									addToResidueString(stringByRes,modifiedResId,string.Format("{0,4}",(resid-1).ToString("D")));
								}
								else if(t.name.Contains("AmideC") && (atom.name.Contains("Nitrogen") || atom.name.Contains("Hydrogen"))){
									addToResidueString(stringByRes,modifiedResId,string.Format("{0,4}",(resid+1).ToString("D")));
								}
								else{
									addToResidueString(stringByRes,modifiedResId,string.Format("{0,4}",resid.ToString("D")));
								}

							}

							addToResidueString(stringByRes,modifiedResId,"     ");
							addToResidueString(stringByRes,modifiedResId,(-pos.x).ToString("F8").Substring(0,7));
							addToResidueString(stringByRes,modifiedResId," ");
							addToResidueString(stringByRes,modifiedResId,pos.y.ToString("F8").Substring(0,7));
							addToResidueString(stringByRes,modifiedResId," ");
							addToResidueString(stringByRes,modifiedResId,pos.z.ToString("F8").Substring(0,7));
							addToResidueString(stringByRes,modifiedResId,"     0     0           ");
							addToResidueString(stringByRes,modifiedResId,atomType);
							addToResidueString(stringByRes,modifiedResId,"  \n");
							nbAtom++;
						}
					}
				}
			}
			
			for(int i=minId;i<=maxId;i++){
				toprint.Append(stringByRes[i].ToString());
			}
			toprint.Append("\nEND");
			Debug.Log(toprint.ToString());
		}
	}


	void addToResidueString(Dictionary<int,StringBuilder> resStrings,int resid,string toadd){
		if(resStrings.ContainsKey(resid)){
			resStrings[resid].Append(toadd);
		}
		else{
			resStrings[resid] = new StringBuilder();
			resStrings[resid].Append(toadd);
		}
	}
}
