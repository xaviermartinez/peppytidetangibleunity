import argparse
import random
import time

from pythonosc import osc_message_builder
from pythonosc import udp_client


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--ip", default="127.0.0.1",
      help="The ip of the OSC server")
  parser.add_argument("--port", type=int, default=5005,
      help="The port the OSC server is listening on")
  args = parser.parse_args()

  client = udp_client.SimpleUDPClient(args.ip, args.port)

  client.send_message("/molego/connect", [5,1])
  client.send_message("/molego/connect", [4,6])

  client.send_message("/molego/connect", [10,2])


  # client.send_message("/molego/disconnect", [1,5])
  client.send_message("/molego/angle", [1,5, 120.0])
  # client.send_message("/molego/vibration", [1,40.0])
  # client.send_message("/molego/light", [1,21312312])