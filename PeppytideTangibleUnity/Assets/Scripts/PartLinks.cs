﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartLinks : MonoBehaviour {

	public MolegoLink.PartType type;

	public GameObject leftLink = null;//Towards N-ter
	public GameObject rightLink = null;//Towards C-ter
	
	public GameObject link3;

	public List<GameObject> getListLeft(){
		List<GameObject> res = new List<GameObject>();

		if(link3 != null){
			res.Add(link3);
		}
		GameObject go = leftLink;

		while(go != null){
			PartLinks pl = go.GetComponent<PartLinks>();

			res.Add(go);
			if(pl.link3 != null){
				res.Add(link3);
			}

			go = pl.leftLink;

			if(go == leftLink){
				Debug.LogWarning("Cycle");
				break;
			}
		}
		return res;
	}
	public List<GameObject> getListRight(){
		List<GameObject> res = new List<GameObject>();

		if(link3 != null){
			res.Add(link3);
		}

		GameObject go = rightLink;
		while(go != null){
			PartLinks pl = go.GetComponent<PartLinks>();

			res.Add(go);
			
			if(pl.link3 != null){
				res.Add(link3);
			}

			go = pl.rightLink;

			if(go == rightLink){
				Debug.LogWarning("Cycle");
				break;
			}
		}
		return res;
	}
}
