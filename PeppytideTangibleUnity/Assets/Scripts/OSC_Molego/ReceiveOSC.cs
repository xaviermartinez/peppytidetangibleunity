﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityOSC;


public class ReceiveOSC : MonoBehaviour {

	private OSCReciever reciever;

	public int port = 5005;

	public Dictionary<int, GameObject> idToGo = new Dictionary<int, GameObject>();

	GameObject peptide;

	public MolegoIds molIds;

	// Use this for initialization
	IEnumerator Start () {

		molIds = new MolegoIds();

		reciever = new OSCReciever();
		reciever.Open(port);

		peptide = new GameObject("PeppytidePeptide");

		// createPart(5);
		// createPart(1);
		// linkPart(5, 1);

		// createPart(4);
		// createPart(6);
		// linkPart(4, 6);

		// createPart(10);
		// createPart(2);
		// linkPart(2, 10);

		// createPart(7);
		// createPart(8);
		// linkPart(7, 8);

		// for (int a = 0; a < 100; a++) {

		// 	for (int i = 0; i < 60; i++)
		// 		yield return new WaitForEndOfFrame();

		// 	setAngle(7, 8, 0.0f);
		// 	// setAngle(5, 1, -110.0f);
		// }


		// for (int i = 0; i < 60; i++)
		// 	yield return new WaitForEndOfFrame();

		// // unlinkPart(2, 10);

		// // linkPart(9, 3);//Cycle

		// setAngle(5, 1, 0.0f);
		// for (int i = 0; i < 60; i++){
		// 	yield return new WaitForEndOfFrame();
		// }
		// Debug.Log("!!!!");
		// setAngle(5, 1, 0.0f);
		// for (int i = 0; i < 30; i++)
		// 	yield return new WaitForEndOfFrame();
		// setAngle(5, 1, 0.0f);
		// for (int i = 0; i < 30; i++)
		// 	yield return new WaitForEndOfFrame();
		// setAngle(5, 1, 0.0f);
		// for (int i = 0; i < 30; i++)
		// 	yield return new WaitForEndOfFrame();
		// setAngle(5, 1, 0.0f);
		// for (int i = 0; i < 30; i++)
		// 	yield return new WaitForEndOfFrame();

		// // for (int a = 0; a < 180; a++) {
		// // 	for (int i = 0; i < 10; i++)
		// // 		yield return new WaitForEndOfFrame();

		// // 	setAngle(5, 1, -(float)a);


		// }

		yield return null;

	}

	void Update () {
		if (reciever.hasWaitingMessages()) {
			OSCMessage msg = reciever.getNextMessage();
			// Debug.Log(string.Format("message received: {0} {1}", msg.Address, DataToString(msg.Data)));
			ParseOSCCommand(msg.Address, msg.Data);
		}
	}


	void ParseOSCCommand(string command, List<object> data) {
		int id1;
		int id2;
		try {
			switch (command) {
			case "/molego/connect":
				if (data.Count != 2) {
					Debug.LogError("connect command should have 2 arguments");
					break;
				}
				id1 = (int)data[0];
				id2 = (int)data[1];
				createPart(id1);
				createPart(id2);
				linkPart(id1, id2);
				break;
			case "/molego/disconnect":
				if (data.Count != 2) {
					Debug.LogError("disconnect command should have 2 arguments");
					break;
				}
				id1 = (int)data[0];
				id2 = (int)data[1];
				unlinkPart(id1, id2);
				break;
			case "/molego/angle":
				if (data.Count != 3) {
					Debug.LogError("angle command should have 3 arguments");
					break;
				}
				id1 = (int)data[0];
				id2 = (int)data[1];
				float angle = (float)data[2];
				setAngle(id1, id2, angle);
				break;
			case "/molego/vibration":
				if (data.Count != 2) {
					Debug.LogError("viration command should have 2 arguments");
					break;
				}
				id1 = (int)data[0];
				float freq = (float)data[1];
				// Debug.Log(id1+" "+freq);
				break;
			case "/molego/light":
				if (data.Count != 2) {
					Debug.LogError("light command should have 2 arguments");
					break;
				}
				id1 = (int)data[0];
				int color = (int)data[1];
				// Debug.Log(id1+" "+color);
				break;

			default:
				Debug.LogError("Unknown command " + command);
				break;
			}
		}
		catch {
			Debug.LogError("Failed to execute command");
		}
	}

	private void createPart(int id) {

		int idM = -1;
		if (molIds.idToType.ContainsKey(id)) {
			idM = molIds.idToType[id].idMol;
		}
		if (idM != -1 && !idToGo.ContainsKey(idM)) {
			GameObject go1 = null;
			if (molIds.idToType.ContainsKey(id)) {
				MolegoLink link = molIds.idToType[id];
				PartLinks lks = null;
				switch (link.partType) {
				case MolegoLink.PartType.Amide:
					go1 = CreatePeppytidePartsPhiPsi.createAmide(peptide.transform, false, false);
					go1.name = "Amide" + idM;
					lks = go1.AddComponent<PartLinks>();
					lks.type = MolegoLink.PartType.Amide;
					break;
				case MolegoLink.PartType.Calpha:
					go1 = CreatePeppytidePartsPhiPsi.createCalpha(peptide.transform, false);
					go1.name = "Calpha" + idM;
					lks = go1.AddComponent<PartLinks>();
					lks.type = MolegoLink.PartType.Calpha;
					break;
				case MolegoLink.PartType.Radical:
					go1 = CreatePeppytidePartsPhiPsi.createSideChain(peptide.transform, "R_AlaninePrefab", false);
					go1.name = "Radical" + idM;
					lks = go1.AddComponent<PartLinks>();
					lks.type = MolegoLink.PartType.Radical;
					break;
				default:
					break;
				}
				idToGo[idM] = go1;

			}
			else {
				Debug.LogError("Type not definied in the Ids");
			}
		}
	}

	private void linkPart(int id1, int id2) {
		int idM1 = -1;
		int idM2 = -1;

		if (molIds.idToType.ContainsKey(id1)) {
			idM1 = molIds.idToType[id1].idMol;
		}
		if (molIds.idToType.ContainsKey(id2)) {
			idM2 = molIds.idToType[id2].idMol;
		}

		if (idM1 != -1 && idM2 != -1 &&
		        idToGo.ContainsKey(idM1) && idToGo.ContainsKey(idM2)) {

			GameObject go1 = idToGo[idM1];
			GameObject go2 = idToGo[idM2];

			if (go1 == go2) {
				Debug.LogError("Cannot link a part with itself");
				return;
			}

			MolegoLink l1 = molIds.idToType[id1];
			MolegoLink l2 = molIds.idToType[id2];

			PartLinks pl1 = go1.GetComponent<PartLinks>();
			PartLinks pl2 = go2.GetComponent<PartLinks>();

			if (pl1.link3 != null && l1.isR() ||
			        pl2.link3 != null && l2.isR()) {
				Debug.LogError("Cannot link " + id1 + " and " + id2 + ", a part is already binded there");
				return;
			}

			//Check if part1 and/or part2 is already connected to other parts
			if ( (!l1.isR() && pl1.leftLink != null && pl1.rightLink != null)
			        || (!l2.isR() && pl2.leftLink != null && pl1.rightLink != null) ) {
				Debug.LogError("Cannot link " + id1 + " and " + id2 + ", a part is already binded there");
				return;
			}

			if (!l1.isR() && pl1.leftLink != null && l1.isLeft() ||
			        !l1.isR() && pl1.rightLink != null && !l1.isLeft() ||
			        !l2.isR() && pl2.leftLink != null && l2.isLeft() ||
			        !l2.isR() && pl2.rightLink != null && !l2.isLeft()) {

				Debug.LogError("Cannot link " + id1 + " and " + id2 + ", a part is already binded there");
				return;
			}

			Transform posL1 = null;
			Transform posL2 = null;
			Transform posL1Int = null;
			Transform posL2Int = null;

			Vector3 axisLink1 = getAxisLinkPart(go1, l1, ref posL1, ref posL1Int);
			Vector3 axisLink2 = getAxisLinkPart(go2, l2, ref posL2, ref posL2Int);



			List<GameObject> gos = null;
			if (!l2.isLeft()) {
				gos = pl2.getListLeft();
				foreach (GameObject l in gos) {
					l.transform.SetParent(go2.transform);
				}
			}
			else {
				gos = pl2.getListRight();
				foreach (GameObject r in gos) {
					r.transform.SetParent(go2.transform);
				}
			}


			Quaternion rotation = Quaternion.FromToRotation(-axisLink2, axisLink1) * go2.transform.rotation;
			go2.transform.rotation = rotation;
			//Apply rotation before getting th translation
			Vector3 translation = go2.transform.position + (posL1.position - posL2.position);
			go2.transform.position = translation;//Glue both parts



			//Restore parents
			if (!l2.isLeft()) {
				gos = pl2.getListLeft();
				foreach (GameObject l in gos) {
					l.transform.SetParent(peptide.transform);
				}
			}
			else {
				gos = pl2.getListRight();
				foreach (GameObject r in gos) {
					r.transform.SetParent(peptide.transform);
				}
			}

			if (l1.isLeft()) {
				pl1.leftLink = go2;
			}
			else {
				pl1.rightLink = go2;
			}

			if (l2.isLeft()) {
				pl2.leftLink = go1;
			}
			else {
				pl2.rightLink = go1;
			}

		}
		else {
			Debug.LogError("Unknown id");
		}
	}
	private void unlinkPart(int id1, int id2) {
		int idM1 = -1;
		int idM2 = -1;

		if (molIds.idToType.ContainsKey(id1)) {
			idM1 = molIds.idToType[id1].idMol;
		}
		if (molIds.idToType.ContainsKey(id2)) {
			idM2 = molIds.idToType[id2].idMol;
		}

		if (idM1 != -1 && idM2 != -1 &&
		        idToGo.ContainsKey(idM1) && idToGo.ContainsKey(idM2)) {

			GameObject go1 = idToGo[idM1];
			GameObject go2 = idToGo[idM2];

			if (go1 == go2) {
				Debug.LogError("Cannot unlink a part with itself");
				return;
			}

			MolegoLink l1 = molIds.idToType[id1];
			MolegoLink l2 = molIds.idToType[id2];

			PartLinks pl1 = go1.GetComponent<PartLinks>();
			PartLinks pl2 = go2.GetComponent<PartLinks>();

			//Test if they are actually connected
			if ( (l1.isLeft() && pl1.leftLink != go2) ||
			        (!l1.isLeft() && pl1.rightLink != go2) ||
			        (l2.isLeft() && pl2.leftLink != go1) ||
			        (!l2.isLeft() && pl2.rightLink != go1) ) {

				Debug.LogError("Parts are not connected");
				return;
			}


			if (l1.isLeft()) {
				pl1.leftLink = null;
			}
			else {
				pl1.rightLink = null;
			}
			if (l2.isLeft()) {
				pl2.leftLink = null;
			}
			else {
				pl2.rightLink = null;
			}

			//Check if part needs to be removed
			if ( (l1.isLeft() && pl1.rightLink == null) ||
			        (!l1.isLeft() && pl1.leftLink == null)) {
				Debug.Log("Remove GameObject " + go1.name);
				foreach(var item in idToGo.Where(kvp => kvp.Value == go1).ToList())
				{
					idToGo.Remove(item.Key);
				}
				GameObject.Destroy(go1);
			}


			if ((l2.isLeft() && pl2.rightLink == null) ||
			        (!l2.isLeft() && pl2.leftLink == null) ) {
				Debug.Log("Remove GameObject " + go2.name);
				foreach(var item in idToGo.Where(kvp => kvp.Value == go2).ToList())
				{
					idToGo.Remove(item.Key);
				}
				GameObject.Destroy(go2);
			}

		}
		else {
			Debug.LogError("Unknown id");
		}
	}

	private void setAngle(int id1, int id2, float angle) {
		int idM1 = -1;
		int idM2 = -1;

		if (molIds.idToType.ContainsKey(id1)) {
			idM1 = molIds.idToType[id1].idMol;
		}
		if (molIds.idToType.ContainsKey(id2)) {
			idM2 = molIds.idToType[id2].idMol;
		}

		if (idM1 != -1 && idM2 != -1 &&
		        idToGo.ContainsKey(idM1) && idToGo.ContainsKey(idM2)) {

			GameObject go1 = idToGo[idM1];
			GameObject go2 = idToGo[idM2];

			if (go1 == go2) {
				Debug.LogError("Cannot set an angle for a part with itself");
				return;
			}

			MolegoLink l1 = molIds.idToType[id1];
			MolegoLink l2 = molIds.idToType[id2];

			PartLinks pl1 = go1.GetComponent<PartLinks>();
			PartLinks pl2 = go2.GetComponent<PartLinks>();

			//Test if they are actually connected
			if ( (l1.isLeft() && pl1.leftLink != go2) ||
			        (!l1.isLeft() && pl1.rightLink != go2) ||
			        (l2.isLeft() && pl2.leftLink != go1) ||
			        (!l2.isLeft() && pl2.rightLink != go1) ) {

				Debug.LogError("Parts are not connected");
				return;
			}


			Transform posL1 = null;
			Transform posL2 = null;
			Transform posL1Int = null;
			Transform posL2Int = null;

			Vector3 axisLink1 = getAxisLinkPart(go1, l1, ref posL1, ref posL1Int);
			Vector3 axisLink2 = getAxisLinkPart(go2, l2, ref posL2, ref posL2Int);


			List<GameObject> gos = null;
			if (!l2.isLeft()) {
				gos = pl2.getListLeft();
				foreach (GameObject l in gos) {
					l.transform.SetParent(go2.transform);
				}
			}
			else {
				gos = pl2.getListRight();
				foreach (GameObject r in gos) {
					r.transform.SetParent(go2.transform);
				}
			}
			Transform a1 = null;
			Transform a2 = null;
			Transform a3 = null;
			Transform a4 = null;

			getAtomsDihedralAngle(go1, go2, l1, l2, ref a1, ref a2, ref a3, ref a4);


			float curAngle = calc_dihedral(a1.position, a2.position, a3.position, a4.position) * Mathf.Rad2Deg;
			float newAngle = angle;


			//Set angle there
			go2.transform.RotateAround(posL2Int.position, -axisLink2, curAngle - newAngle);



			//Restore parents
			if (!l2.isLeft()) {
				gos = pl2.getListLeft();
				foreach (GameObject l in gos) {
					l.transform.SetParent(peptide.transform);
				}
			}
			else {
				gos = pl2.getListRight();
				foreach (GameObject r in gos) {
					r.transform.SetParent(peptide.transform);
				}
			}
		}
		else {
			Debug.LogError("Unknown id");
		}
	}

	private Vector3 getAxisLinkPart(GameObject go, MolegoLink l, ref Transform posExt, ref Transform posInt) {
		Vector3 result = Vector3.zero;
		switch (l.partType) {
		case MolegoLink.PartType.Amide:
			if (l.linkType == MolegoLink.LinkType.C) {
				Transform tin = go.transform.Find("Links/intlink2");
				Transform tout = go.transform.Find("Links/extlink2C");
				posExt = tout;
				posInt = tin;
				return tout.position - tin.position;
			}
			else if (l.linkType == MolegoLink.LinkType.N) {
				Transform tin = go.transform.Find("Links/intlink1");
				Transform tout = go.transform.Find("Links/extlink1N");
				posExt = tout;
				posInt = tin;
				return tout.position - tin.position;
			}
			break;
		case MolegoLink.PartType.Calpha:
			if (l.linkType == MolegoLink.LinkType.C) {
				Transform CaLinkInt = go.transform.Find("Links/intlink1");
				Transform CaLinkExt = go.transform.Find("Links/extlink1C");
				posExt = CaLinkExt;
				posInt = CaLinkInt;
				return CaLinkExt.position - CaLinkInt.position;
			}

			else if (l.linkType == MolegoLink.LinkType.N) {
				Transform CaLinkInt = go.transform.Find("Links/intlink2");
				Transform CaLinkExt = go.transform.Find("Links/extlink2N");
				posExt = CaLinkExt;
				posInt = CaLinkInt;
				return CaLinkExt.position - CaLinkInt.position;
			}

			else if (l.linkType == MolegoLink.LinkType.R) {
				Transform CaLinkInt = go.transform.Find("Links/intlink3");
				Transform CaLinkExt = go.transform.Find("Links/extlink3M");
				posExt = CaLinkExt;
				posInt = CaLinkInt;
				return CaLinkExt.position - CaLinkInt.position;
			}

			break;
		case MolegoLink.PartType.Radical:
			Transform sclinkint = go.transform.Find("Links/intlinkC1");
			Transform sclinkext = go.transform.Find("Links/extlinkC1");
			posExt = sclinkext;
			posInt = sclinkint;
			return sclinkext.position - sclinkint.position;
			break;
		}
		return result;
	}

	private void getAtomsDihedralAngle(GameObject go1, GameObject go2,
	                                   MolegoLink l1, MolegoLink l2,
	                                   ref Transform a1, ref Transform a2,
	                                   ref Transform a3, ref Transform a4) {

		switch (l1.partType) {
		case MolegoLink.PartType.Amide:
			if (l1.linkType == MolegoLink.LinkType.C) {
				a1 = go1.transform.Find("Centers/N_center");
				a2 = go1.transform.Find("Centers/C_center");
				a3 = go1.transform.Find("Centers/CA_C_center");
			}
			else if (l1.linkType == MolegoLink.LinkType.N) {
				a1 = go1.transform.Find("Centers/C_center");
				a2 = go1.transform.Find("Centers/N_center");
				a3 = go1.transform.Find("Centers/CA_N_center");
			}
			break;
		case MolegoLink.PartType.Calpha:
			if (l1.linkType == MolegoLink.LinkType.C) {
				a1 = go1.transform.Find("Centers/AmideN_center");
				a2 = go1.transform.Find("Centers/C_center");
				a3 = go1.transform.Find("Centers/AmideC_center");
			}
			else if (l1.linkType == MolegoLink.LinkType.N) {
				a1 = go1.transform.Find("Centers/AmideN_center");
				a2 = go1.transform.Find("Centers/C_center");
				a3 = go1.transform.Find("Centers/AmideC_center");
			}

			else if (l1.linkType == MolegoLink.LinkType.R) {
				a1 = go1.transform.Find("Centers/H_center");
				a2 = go1.transform.Find("Centers/C_center");
				a3 = go1.transform.Find("Centers/R_center");
			}

			break;
		case MolegoLink.PartType.Radical:
			//Warning only works for Alanine!!!!
			a1 = go1.transform.Find("Centers/H1_center");
			a2 = go1.transform.Find("Centers/C_center");
			a3 = go2.transform.Find("Centers/C_center");
			break;
		}

		switch (l2.partType) {
		case MolegoLink.PartType.Amide:
			if (l2.linkType == MolegoLink.LinkType.C) {
				a4 = go2.transform.Find("Centers/N_center");
			}
			else if (l2.linkType == MolegoLink.LinkType.N) {
				a4 = go2.transform.Find("Centers/C_center");
			}
			break;
		case MolegoLink.PartType.Calpha:
			if (l2.linkType == MolegoLink.LinkType.C) {
				a4 = go2.transform.Find("Centers/AmideC_center");
			}

			else if (l2.linkType == MolegoLink.LinkType.N) {
				a4 = go2.transform.Find("Centers/AmideN_center");
			}

			else if (l2.linkType == MolegoLink.LinkType.R) {
				a4 = go2.transform.Find("Centers/R_center");
			}

			break;
		case MolegoLink.PartType.Radical:
			//Warning only works for Alanine!!!!
			a4 = go2.transform.Find("Centers/H1_center");
			break;
		}
	}

	float calc_dihedral(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4) {
		Vector3 ab = v1 - v2;
		Vector3 cb = v3 - v2;
		Vector3 db = v4 - v3;

		Vector3 u = Vector3.Cross(ab, cb);
		Vector3 v = Vector3.Cross(db, cb);
		Vector3 w = Vector3.Cross(v, u);

		float angle = get_angle2(u, v);
		if (get_angle2(cb, w) > 0.001f)
			angle = -angle;
		return angle;

	}
	float get_angle2(Vector3 v1, Vector3 v2) {
		float s = Vector3.Cross(v1, v2).magnitude;
		float c = Vector3.Dot(v1, v2);
		float angle = Mathf.Atan2(s, c);
		return angle;
	}


}
